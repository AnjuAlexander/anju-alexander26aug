public class TrianingPageController {

    public List<Contact> contactList{get;set;}
    public Boolean renderedSec{get;set;}
    public Contact newContact{get;set;}
    public Boolean showNewSec{get;set;}
    Account a;
        public TrianingPageController(ApexPages.StandardController controller) {
            a=(Account)controller.getRecord();
           contactList=new List<Contact>();
           renderedSec=false;
           contactList=[select id,lastname,email from contact where accountId=:a.id];
           if(contactList.size()>0)
           renderedSec=true;
           showNewSec=false;
           //System.assertEquals(ContactList,null);
    }
    public void createNewContact(){
        newContact=new Contact();
        showNewSec=true;
    }
     public void saveContact(){
       newContact.AccountId=a.id;
              insert newContact;
    }
}