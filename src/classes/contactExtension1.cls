public class contactExtension1 {

public Boolean show{get;set;}
public String accDetails{get;set;}
public Account selectAccount{get;set;}
    private final Account a; 
    public contactExtension1(ApexPages.StandardController stdController) {
        this.a = (Account)stdController.getRecord();
    }
  
  public List<selectOption> getaccts() {
        List<selectOption> options = new List<selectOption>(); 

        options.add(new selectOption('', '- None -')); 

        for (Account account : [SELECT Id, Name FROM Account LIMIT  5]) { 
            options.add(new selectOption(account.id, account.Name)); 

        }
        return options; 
    }
    
    public pageReference accts()
    {
    selectAccount=[select id,name from account LIMIT 1];
    show=true;
    return null;
    
    }
    
    
    
}