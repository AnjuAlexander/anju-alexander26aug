public with sharing class DependantPicklistCon
{

// Create strings to put our values in from the page
public string strRecordType         {get;set;}

private final Account Acc;

// The extension constructor initializes the private member
// variable Acc by using the getRecord method from the standard
// controller.
public DependantPicklistCon(ApexPages.StandardController stdController)
{   
    this.Acc = (Account)stdController.getRecord();
}

//Create our list of record types
public list<SelectOption> getRecordTypes()
{
    list<SelectOption> options = new list<SelectOption>();

    for(RecordType sRecordType : [SELECT Id, Name FROM RecordType WHERE sObjectType = 'Account'])
    {
        // Select options are added with key,value pairing
        options.add(new SelectOption(sRecordType.Id, sRecordType.Name));
    }
    return options;
}

public void updateRecordType()
{
    Acc.RecordTypeId = strRecordType;
}

}