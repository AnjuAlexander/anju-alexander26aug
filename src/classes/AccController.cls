public class AccController {

    public Boolean showThisSec { get; set; }

    public Account account { get; set; }
    public String selectedAccId{get;set;}
    
     public PageReference detailAcc() {
     account=[select id,name,Phone from account where Id=:selectedAccId];
     //System.assertEquals(account,null);
     showThisSec =true;
        return null;
    }

       public List<SelectOption> getAccountNames() {       
              List<SelectOption> accOptions= new List<SelectOption>();
              
              accOptions.add( new SelectOption('','--Select--'));
             
              for( Account acc : [select Id,name from Account ] ) {
                      accOptions.add( new SelectOption(acc.Id,acc.name)); /*SelectOption list takes two parameters one is value and other one is label .In this case account name as a label and Id is the value .*/
              }
             return accOptions;
       }
}